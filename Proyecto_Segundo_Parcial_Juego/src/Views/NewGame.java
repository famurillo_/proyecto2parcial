/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import game.Player;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Darinka
 */
public class NewGame {
    
    private Date fecha;
    private VBox root;
    private HBox container;
    private HBox container2;
    private Label nameLabel;
    private TextField nameField;
    private Button play;
    private Button regresar;
    private Player jugador;
    private Scene scene;
    private final Stage previous;
    
    public NewGame(Stage s) {
        previous = s;
        inicialize();
    }

    private void inicialize() {

        //ORGANIZE NODES
        nameLabel = new Label("Nombre: ");
        nameLabel.setId("name_new");
        nameField = new TextField();
        nameField.setId("txt_field");
        play = new Button("Jugar");
        play.setId("but_new");
        
        regresar = new Button("Regresar");
        regresar.setId("but_new");
        play.setOnAction(e -> abrirVentana());
        regresar.setOnAction(e-> regresar());
        container = new HBox(50, nameLabel, nameField);
        container.setAlignment(Pos.CENTER);
        container2 = new HBox(play, regresar);
        container2.setAlignment(Pos.CENTER);
        root = new VBox(50, container, container2);
        root.setAlignment(Pos.CENTER);
        root.setId("root_new");
        
        scene = new Scene(root, 800, 500);
        scene.getStylesheets().add("/Views/stylesCSS.css");

    }

    public Scene getScene() {
        return scene;
    }

   
    private void regresar(){
        previous.setScene(new Menu(previous).getScene());
    }
    
  

    public void abrirVentana() {
        jugador = new Player(nameField.getText());
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
        String strDate = dateFormat.format(date);
        jugador.setDate(strDate);
        previous.setScene(new Level1(jugador, previous).getScene());
    }
    
    
}

