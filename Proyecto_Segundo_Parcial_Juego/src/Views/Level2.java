/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import game.Player;
import game.Position;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author Darinka
 */
public class Level2 extends Level{

    public Level2(Player jugador, Stage previous) {
        super(jugador, previous);
        añadirObstaculos();
    }

    @Override
    protected void savePlayer() {
        jugador.setLevel(2);
        players.add(jugador);
        serialize();
    }

    @Override
    protected void win() {
        savePlayer();
        Thread.currentThread().interrupt();
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Ganador");
        a.setHeaderText("Has completado el juego. ¡FELICIDADES!");
        a.setContentText("Jugador: " + jugador.getName()+ "\nPuntaje: " + jugador.getPoints());

        // option != null.
        Optional<ButtonType> option = a.showAndWait();

        if (option.get() == ButtonType.OK) {
           s.setScene(new Menu(s).getScene());
        }
    }

    @Override
    protected void setDelay() {
        delay = 250;
    }
    
    private void añadirObstaculos(){
        Position p = getRandomPos();
        board.add(new ImageView(new Image("/images/oruga.png", 50, 50, true, true)), (int) p.getX(), (int)p.getY());
        Position p2 = getRandomPos();
        board.add(new ImageView(new Image("/images/oruga.png", 50, 50, true, true)), (int) p2.getX(), (int)p2.getY());
        Position p3 = getRandomPos();
        board.add(new ImageView(new Image("/images/oruga.png", 50, 50, true, true)), (int) p3.getX(), (int)p3.getY());
        Position p4 = getRandomPos();
        board.add(new ImageView(new Image("/images/oruga.png", 50, 50, true, true)), (int) p4.getX(), (int)p4.getY());
        
        Position p11 = getRandomPos();
        board.add(new ImageView(new Image("/images/hoja.png", 50, 50, true, true)), (int) p11.getX(), (int)p11.getY());
        Position p22 = getRandomPos();
        board.add(new ImageView(new Image("/images/hoja.png", 50, 50, true, true)), (int) p22.getX(), (int)p22.getY());
        Position p33 = getRandomPos();
        board.add(new ImageView(new Image("/images/hoja.png", 50, 50, true, true)), (int) p33.getX(), (int)p33.getY());
        Position p44 = getRandomPos();
        board.add(new ImageView(new Image("/images/hoja.png", 50, 50, true, true)), (int) p44.getX(), (int)p44.getY());
        
        
    }
    
   

}
