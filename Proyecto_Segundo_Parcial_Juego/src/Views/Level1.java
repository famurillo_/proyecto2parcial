/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import game.Player;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

/**
 *
 * @author Darinka
 */
public class Level1 extends Level{

    public Level1(Player jugador, Stage previous) {
        super(jugador, previous);
        
    }

    @Override
    protected void savePlayer() {
        jugador.setLevel(1);
        players.add(jugador);
        serialize();
    }

    @Override
    protected void win() {
        Thread.currentThread().interrupt();
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Nivel completado");
        a.setHeaderText("Has completado el nivel. ¡FELICIDADES!");
        a.setContentText("¿Deseas continuar con el siguiente nivel?");

        // option != null.
        Optional<ButtonType> option = a.showAndWait();

        if (option.get() == ButtonType.OK) {
           s.setScene(new Level2(jugador, s).getScene());
        }else if (option.get() == ButtonType.CANCEL){
            savePlayer();
        }
    }

    @Override
    protected void setDelay() {
        delay = 1000;
    }

    
}
