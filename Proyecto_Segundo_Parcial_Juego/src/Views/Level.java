package Views;

import game.Ant;
import game.Fly;
import game.Lizard;
import game.Player;
import game.Position;
import game.Spider;
import game.Status;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import threads.Chronometer;
import threads.HiloHormiga;
import threads.HiloLagartija;
import threads.HiloMosca;
import threads.HiloRecompensa;

/**
 *
 * @author Darinka
 */
public abstract class Level {
    
    protected int delay;
    
    public static boolean stopLizard = false;
    protected ArrayList<Player> players;
    protected final int MAXCOL = 10;
    protected final int MAXROW = 10;
    protected final int NUMMOS = 4;
    protected final int NUMHOR = 5;
    protected final int ANTPOINTS = 10;
    protected final int FLYPOINTS = 15;
    
    protected Player jugador;
    
    protected Map<ImageView, Position> objetos;
    protected ArrayList<ImageView> allObs;
    protected ArrayList<Fly> moscas;
    protected ArrayList<Ant> hormigas;
    protected ArrayList<ImageView> corazones;
    protected Spider araña;
    protected Lizard lagartija;
    protected int puntos;
    protected int vidas;
    protected Chronometer crono;
    
    protected Rectangle r_jugador;
    protected Rectangle r_lagartija;
    
    ImageView playeri;
    
    
    protected GridPane board;
    protected FlowPane upper;
    protected VBox root;
    protected StackPane timeStack;
    protected Rectangle timeRect;
    protected Label timeLabel;
    protected FlowPane boxCora;
    protected Label puntosLabel;
    protected Button salir;
    protected Stage s;
    protected Scene scene;
    
    protected final EventHandler<javafx.scene.input.KeyEvent> handler = (javafx.scene.input.KeyEvent event) -> {
        
        if (null != event.getCode()) {
            System.out.println(objetos.keySet());
            if(hormigas.size()== 0 && moscas.size()==0){
                win();
            }
            switch (event.getCode()) {
                case RIGHT:
                    playeri.setTranslateX(playeri.getTranslateX() + 10);
                    choqueObs();
                    break;
                case LEFT:
                    playeri.setTranslateX(playeri.getTranslateX() - 10);
                    choqueObs();
                    break;
                case UP:
                    playeri.setTranslateY(playeri.getTranslateY() - 10);
                    choqueObs();
                    break;
                case DOWN:
                    playeri.setTranslateY(playeri.getTranslateY() + 10);
                    choqueObs();
                    break;
                default:
                    break;
            }
        }
        event.consume();
    };
    
    public Level(Player jugador, Stage previous){
        
        s = previous;
        this.jugador = jugador;
        initialize();
        setDelay();
        giveStyles();
        desserialize();
        setBoardConstraints();
        fillHearts();
        showHearts();
        ubicarJugador();
        ubicarPredator();
        ubicarPresas();
        addReward();
        iniciarTiempo();
        showReward();
    }
    
    /*
    TODO: Revisar posición jugador e hilo tiempo
    */
    protected void initialize(){
        delay = 500;
        players = new ArrayList<>();
        corazones = new ArrayList<>();
        objetos = new HashMap<>();
        moscas = new ArrayList<>();
        hormigas = new ArrayList<>();
        araña = new Spider("s1", "/images/spider.png", getRandomPos(), Status.ALIVE);
        lagartija = new Lizard("lz1", "/images/lizard.png", getRandomPos(), Status.ALIVE);
        puntos = 0;
        vidas = 3;
        crono = new Chronometer(timeLabel);
        timeLabel = new Label();
        timeRect = new Rectangle(50, 50, Paint.valueOf("pink"));
        timeStack = new StackPane(timeRect, timeLabel);
        puntosLabel = new Label(String.valueOf(puntos));
        
        r_jugador = new Rectangle(70, 70, new ImagePattern(new Image(araña.getPath())));
        r_lagartija = new Rectangle(70, 70, new ImagePattern(new Image(lagartija.getPath())));
        
        salir = new Button("Salir");
        salir.setOnAction(e-> {
            s.setScene(new Menu(s).getScene());
        });
        board = new GridPane();
        
        boxCora = new FlowPane(Orientation.HORIZONTAL);
        upper = new FlowPane(Orientation.HORIZONTAL, timeStack, boxCora, puntosLabel, salir);
        upper.setAlignment(Pos.CENTER);
        root = new VBox(upper, board);
        scene = new Scene(root, 800, 800);
        scene.setOnKeyPressed(handler);
    }
    
    protected void giveStyles(){
        
        salir.setId("but_new");
         board.setId("pane");
         timeLabel.setId("title_main");
         puntosLabel.setId("title_main");
        scene.getStylesheets().add("/Views/stylesCSS.css");
    }
    
    protected Position getRandomPos(){
        Position p = new Position(new Random().nextInt(10), new Random().nextInt(10));
        while(objetos.values().contains(p)){
            p = new Position(new Random().nextInt(10), new Random().nextInt(10));
        } 
        
        return p;
    }

    public Scene getScene() {
        return scene;
    }
    
    
    
    public  void choqueObs() {
        System.out.println("choqueObs......");
        for (ImageView r : objetos.keySet()) {
            if (playeri.getBoundsInParent().intersects(r.getBoundsInParent())) {

                switch (r.getId()) {
                    case "ant":
                        System.out.println("ANNNT");
                        puntos+=ANTPOINTS;
                        jugador.setPoints(jugador.getPoints() + puntos);
                        hormigas.remove(hormigas.size()-1);
                        board.getChildren().remove(r);
                        break;
                    case "lizard":
                        System.out.println("LIZAAARD");
                        if(corazones.size()==0){
                            lose();
                            savePlayer();
                        }else{
                            vidas--;
                            corazones.remove(corazones.size()-1);
                            showHearts();
                        }
                        
                        break;
                    case "fly":
                        System.out.println("flyyyy");
                        puntos+=FLYPOINTS;
                        jugador.setPoints(jugador.getPoints() + puntos);
                        moscas.remove(moscas.size()-1);
                        board.getChildren().remove(r);
                        break;
                    case "reward":
                        System.out.println("REWAARD");
                        board.getChildren().remove(r);
                        stopLizard = true;
                        break;
                        
                     
                    default:
                        break;
                }

            }

        }
    }
    
    
    protected void fillHearts(){
        for(int i = 0; i<3; i++){
            corazones.add(new ImageView(new Image("/images/corazon.png", 50, 50, true, true)));
        }
    }
    
    protected void showHearts(){
        for(ImageView i: corazones){
            boxCora.getChildren().add(i);
        }
    }
    
    protected void setBoardConstraints(){
        for(int i = 0; i<MAXCOL; i++){
            board.getColumnConstraints().add(new ColumnConstraints(70));
        }
        for(int i = 0; i<MAXROW; i++){
            board.getRowConstraints().add(new RowConstraints(70));
        }
    }
    
    protected void ubicarPresas(){
        for(int i = 0; i<NUMHOR; i++){
            Ant n = new Ant("ant", "/images/ant.png", getRandomPos(), Status.ALIVE);
            hormigas.add(n);
            ImageView im = new ImageView(new Image(n.getPath(), 50, 50, true, true));
            im.setId("ant");
            objetos.put(im, n.getPosition());
            board.add(im, (int) n.getPosition().getX(), (int) n.getPosition().getY());
            System.out.println(i);
            System.out.println(n.getPosition());
            Thread t = new Thread(new HiloHormiga(n, 120, im, delay));
            t.start();
        }
        for(int i = 0; i<NUMMOS; i++){
            Fly f = new Fly("fly", "/images/fly1.png", getRandomPos(), Status.ALIVE);
            moscas.add(f);
            ImageView im = new ImageView(new Image(f.getPath(), 50, 50, true, true));
            im.setId("fly");
            objetos.put(im, f.getPosition());
            board.add(im, (int) f.getPosition().getX(), (int) f.getPosition().getY());
            System.out.println(i);
            System.out.println(f.getPosition());
            Thread t = new Thread(new HiloMosca(f, 120, im, delay));
            t.start();
        }
    }
    
    protected void ubicarJugador(){
        Spider n = new Spider("sp1", "/images/spider.png", new Position(5,5), Status.ALIVE);
        playeri = new ImageView(new Image(n.getPath(), 50, 50, true, true));
        objetos.put(playeri, n.getPosition());
        board.add(playeri, (int) n.getPosition().getX(), (int) n.getPosition().getY());
    }
    
    protected void ubicarPredator(){
        Lizard n = new Lizard("lz1", "/images/lizard.png", getPredatorRPOs(), Status.ALIVE);
        ImageView im = new ImageView(new Image(n.getPath(), 50, 50, true, true));
        im.setId("lizard");
        objetos.put(im, n.getPosition());
        board.add(im, (int) n.getPosition().getX(), (int) n.getPosition().getY());
        Thread t = new Thread(new HiloLagartija(n, 120, im, delay));
        t.start();
    }
    
    protected Position getPredatorRPOs(){
        ArrayList<Position> pos = new ArrayList<>();
        pos.add(new Position(0,9));
        pos.add(new Position(9,0));
        pos.add(new Position(0,0));
        pos.add(new Position(9,9));
        
        return pos.get(new Random().nextInt(4));        
    }
    
    protected void iniciarTiempo(){
        Thread t = new Thread(new Chronometer(timeLabel));
        t.start();
    }
    
    
    protected void lose(){
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Fin del juego");
        a.setContentText("Ha sido devorado por una lagartija 3 veces");
        a.showAndWait();
        s.setScene(new Menu(s).getScene());
    }
    
    protected void serialize(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("players.ser"))){
            
            oos.writeObject(players);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    protected void desserialize(){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("players.ser"))){
            players  = (ArrayList<Player>) ois.readObject();
        } catch (IOException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    protected abstract void savePlayer();
    
    protected void addReward(){
        ImageView im = new ImageView(new Image("/images/recompensa.png", 50, 50, true, true));
        im.setId("reward");
        objetos.put(im, null);
    }
    
    protected void showReward(){
        Thread t = new Thread(new HiloRecompensa(getRandomPos(), board, objetos));
        
        t.start();
    }
    
    public EventHandler<KeyEvent> getHandler() {
        return handler;
    }
    protected abstract void setDelay();
    
    protected abstract void win();
}

