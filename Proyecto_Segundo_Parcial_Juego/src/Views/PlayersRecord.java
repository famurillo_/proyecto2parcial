/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import game.Player;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Darinka
 */
public class PlayersRecord {
    private Label title;
    private TableView listView;
    private VBox root;
    private Button regresar;
    private Stage s;
    private ObservableList<Player> ob = FXCollections.observableArrayList();
    private Scene scene;
    
    public PlayersRecord(Stage s) {
        this.s =s;
        inicializar();
        setTableView();
    }

    private void inicializar() {
        title = new Label("Jugadores");
        regresar = new Button("Regresar");
        regresar.setId("but_new");
        regresar.setOnAction(e-> {
            s.setScene(new Menu(s).getScene());
        } );
        listView = new TableView();
        listView.setId("table");
        root = new VBox(title, listView, regresar);
        scene = new Scene(root, 500, 500);
        
        scene.getStylesheets().add("/Views/stylesCSS.css");
    }

    public Scene getScene() {
        return scene;
    }

    

    public ArrayList<Player> getPlayers() {
        ArrayList<Player> players = new ArrayList<>();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("players.ser"))){
            players  = (ArrayList<Player>) ois.readObject();
        } catch (IOException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Level.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return players;
    }

    private void setTableView() {
        TableColumn<Player, String> nameCol = new TableColumn<>("Nombre");
        nameCol.setMinWidth(200);
        nameCol.setCellValueFactory(new PropertyValueFactory("name"));

        TableColumn<Player, Integer> scoreCol = new TableColumn<>("Puntaje");
        scoreCol.setMinWidth(100);
        scoreCol.setCellValueFactory(new PropertyValueFactory("points"));

        TableColumn<Player, Integer> levelcol = new TableColumn<>("Nivel");
        levelcol.setMinWidth(100);
        levelcol.setCellValueFactory(new PropertyValueFactory("level"));

        TableColumn<Player, String> dateCol = new TableColumn<>("Fecha");
        dateCol.setMinWidth(400);
        dateCol.setCellValueFactory(new PropertyValueFactory("date"));

        ob.addAll(getPlayers());
        listView.setItems(ob);
        listView.getColumns().addAll(nameCol, scoreCol, levelcol, dateCol);

    }
}
