/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Darinka
 */
public class Menu {
    private VBox root;
    private Button user;
    private Button historial;
    private Button exit;
    private ImageView title;
    private Scene scene;
    private Stage stage2;
    

    public Menu(Stage s) {
        inicializar();
        giveActions();
        stage2 = s;
        stage2.setResizable(true);
    }
    
    private void inicializar(){
        exit = new Button("Salir");
        exit.setId("but_new");
        historial = new Button("Historial");
        historial.setId("but_new");
        user = new Button("Nuevo Juego");
        user.setId("but_new");
        title = new ImageView("/images/logo2.png");
        title.setFitHeight(60);
        title.setFitWidth(400);
        
        root = new VBox(title, user, historial, exit);
        root.setAlignment(Pos.CENTER);
        root.setSpacing(50);
        root.setId("root_main");    
        
        scene = new Scene(root, 500, 600);
        scene.getStylesheets().add("/Views/stylesCSS.css");
    }

    public Scene getScene() {
        return scene;
    }
    

    
    
    public void nuevoUsuario(){
        stage2.setScene(new NewGame(stage2).getScene());
    }
    public void mostrarHistorial(){
        stage2.setScene(new PlayersRecord(stage2).getScene());
    }
    public void salir(){
        Platform.exit();
    }
    
    private void giveActions(){
        user.setOnAction(e-> nuevoUsuario());
        historial.setOnAction(e -> mostrarHistorial());
        exit.setOnAction(e-> salir());
    }
}

