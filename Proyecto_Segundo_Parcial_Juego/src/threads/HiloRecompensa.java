/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import game.Position;
import game.Recompensa;
import java.util.Map;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author user
 */
public class HiloRecompensa implements Runnable{
    private Recompensa reward;
    private GridPane pane;
    private Position pos;
    private ImageView im;
    private Map<ImageView, Position> obs;

    public HiloRecompensa(Position pos, GridPane pane, Map<ImageView, Position> obs) {
        this.reward = new Recompensa("/images/recompensa.png", pos);
        this.pane = pane;
        this.pos = pos;
        this.im = new ImageView(new Image(reward.getRuta(), 50, 50, true, true));
        this.im.setId("reward");
        this.obs = obs;
    }

    @Override
    public void run() {
        int i = 0;
        while (i< reward.getTiempo()) {
            try {
                if (i == 0) {
                    Platform.runLater(() -> add());
                } else if (i == reward.getTiempo()) {
                    Platform.runLater(() -> extract());
                }

                Thread.sleep(1000);
                i++;

            } catch (Exception ex) {
                System.err.println("Interrupción en los segundos");
            }
        }
    }

    private void add(){
        pane.add(im, (int)pos.getX(), (int)pos.getY());
    }
    
    private void extract(){
        pane.getChildren().remove(im);
        obs.remove(im , pos);
    }

    public ImageView getIm() {
        return im;
    }
    
    
}
