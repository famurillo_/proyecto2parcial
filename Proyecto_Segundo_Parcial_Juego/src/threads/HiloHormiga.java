/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import game.Ant;
import java.util.Random;
import javafx.application.Platform;
import javafx.scene.image.ImageView;

/**
 *
 * @author user
 */
public class HiloHormiga implements Runnable {
    
    private Ant hormiga;
    private int tiempo;
    private ImageView image;
    private boolean swift;
    private boolean swiftN;
    private int delay;

    public HiloHormiga(Ant hormiga, int tiempo, ImageView image, int delay) {
        this.hormiga = hormiga;
        this.tiempo = tiempo;
        this.image = image;
        this.swift = false;
        this.swiftN = false;
        this.delay = delay;
    }
    

    @Override
    public void run() {
        int i = 0;
        while (i<tiempo) {
            try {
               
                Thread.sleep(delay);
                i++;
                Platform.runLater(()->mover());

            } catch (Exception ex) {
                System.err.println("Interrupción en hilo hormiga");
                return;
            }
        }
    }
    
    private int getMov(){
        int n = new Random().nextInt(15);
        if(swiftN){
            n = -n;
        }
        return n;
    }
    
    private void mover(){
        if(swift){
            image.setTranslateX(image.getTranslateX() + (getMov()));
            swift = false;
        }else{
            image.setTranslateX(image.getTranslateY() + (getMov()));
            swift = true;
        }
    }
    
}
