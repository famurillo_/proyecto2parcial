/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import Views.Level;
import game.Ant;
import game.Lizard;
import java.util.Random;
import javafx.application.Platform;
import javafx.scene.image.ImageView;

/**
 *
 * @author user
 */
public class HiloLagartija implements Runnable{

    private Lizard lagartija;
    private int tiempo;
    private ImageView image;
    private boolean swift;
    private boolean swiftN;
    private int delay;

    public HiloLagartija(Lizard lagartija, int tiempo, ImageView image, int delay) {
        this.lagartija = lagartija;
        this.tiempo = tiempo;
        this.image = image;
        this.swift = false;
        this.swiftN = false;
        this.delay =  delay;
    }
    

    @Override
    public void run() {
        int i = 0;
        while (i<tiempo) {
            try {
               
                Thread.sleep(delay);
                i++;
                if(Level.stopLizard == true){
                    Thread.sleep(5000);
                }
                Platform.runLater(()->mover());

            } catch (Exception ex) {
                System.err.println("Interrupción en hilo hormiga");
                return;
            }
        }
    }
    
    private int getMov(){
        int n = new Random().nextInt(15);
        if(swiftN){
            n = -n;
        }
        return n;
    }
    
    private void mover(){
        if(swift){
            image.setTranslateX(image.getTranslateX() + (getMov()));
            swift = false;
        }else{
            image.setTranslateX(image.getTranslateY() + (getMov()));
            swift = true;
        }
    }
    
}
