/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author user
 */
public class Chronometer implements Runnable{

    private int tiempo;
    Label timeLabel;

    public Chronometer(Label timeLabel) {
        this.timeLabel = timeLabel;
        this.tiempo = 0;
    }

    @Override
    public void run() {
        while (tiempo<120) {
            try {
                
                Thread.sleep(1000);
                tiempo++;
                System.out.println(tiempo);
                Platform.runLater(() -> setTime(tiempo));

            } catch (Exception ex) {
                System.err.println("Interrupción en los segundos");
                return;
            }
        }
    }

    public void setTime(int i) {
        timeLabel.setText(String.valueOf(i));
    }

    
    public int getTiempo(){
        return tiempo;
    }
}
